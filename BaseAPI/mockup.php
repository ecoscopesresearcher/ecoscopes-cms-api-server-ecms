<?php
define('EMONCMS_EXEC', 1);
// 1) Load settings and core scripts
require "process_settings.php";
require "core.php";
require "Modules/log/EmonLogger.php";
$jsondata = array(
'Current'=> 6.1513056755066,
'GraphUnit'=> "kW",
'GraphValue'=> 1.3785733642578,
'Input1High'=> 0,
'Input1Low'=> 0,
'Input2High'=> 0,
'Input2Low'=> 0,
'KW'=> 1.3785733642578,
'MeterCounter'=> 6,
'NullCounter'=> 0,
'Output1Status'=> 0,
'Output2Status'=> 0,
'PF'=> 0.99162250757217,
'PowerRatio'=> "",
'PowerUnit'=> "KWh",
'PulseCounter'=> "",
'PulseLabel'=> "",
'Volts'=> 227.80151367188,
'carbon'=> "0.66kg",
'cost'=> "Ã Â¸Â¿5.51",
'currencySymbol'=> "Ã Â¸Â¿",
'customMessage'=> "It is a Real-time Energy Display! It shows us exactly how much energy we are using...as we use it.↵↵Energy use is normally invisible, that is why it is so easy to waste it. However, this display shows us how our behaviour impacts our energy use and by extension our carbon footprint.↵↵How can you help?↵↵By keeping it green! The main dial will turn orange when we are using too much energy so please do whatever you can to avoid wasting energy and keep our display green!↵↵Thank you!↵↵",
'customMessageDownTime'=> 60,
'customMessageStatus'=> 0,
'customMessageTitle'=> "What is this?",
'customMessageUpTime'=> 15,
'ipAddress'=> "192.168.1.103",
'kWAlarm'=> 0,
'logo'=> "/public/user/1440469911.png",
'pfAlarm'=> 0,
'renderId'=> "71",
'voltsAlarm'=> 0,
'voltsAlarm2'=> 0,
);
header('Content-Type: application/json');
print json_encode($jsondata);