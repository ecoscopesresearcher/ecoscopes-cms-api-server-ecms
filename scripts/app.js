/// <reference path="controllers/solarsystem.js" />
/// <reference path="controllers/solarsystem.js" />
'use strict';

/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.
 */
angular
  .module('yapp', [
     'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'ngAnimate'
  ])
  .config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.when('/dashboard', '/dashboard/overview');
    $urlRouterProvider.otherwise('/home');

    $stateProvider
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
      })
        .state('home', {
          url: '/home',
          parent: 'base',
          templateUrl: 'views/home.html',
          controller: 'HomeCtrl',
          resolve: {
              loadMyFiles: function ($ocLazyLoad) {
                  return $ocLazyLoad.load({
                      name: 'yapp',
                      files: [
                      '/scripts/controllers/home.js',
                      '/scripts/owl.carousel.min.js'                     
                      ]
                  })
              }
          }
      })
        .state('login', {
          url: '/login',
          parent: 'base',
          templateUrl: 'views/login.html',
          controller: 'LoginCtrl'
        })
        .state('dashboard', {
          url: '/dashboard',
          parent: 'base',
          templateUrl: 'views/dashboard.html',
          controller: 'DashboardCtrl'
        })
          .state('overview', {
            url: '/overview',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/overview.html'
          })
          .state('reports', {
            url: '/reports',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/reports.html'
          }).state('solarsystem', {
              url: '/solarsystem',
              parent: 'base',
              templateUrl: 'views/solarsystem.html',
              controller: 'SolarSysCtrl'
          })
          .state('solarsystem.dashboard', {
              url: '/solarsystem/dashboard',
              parent: 'solarsystem',
              templateUrl: 'views/solarsystem/dashboard.html',
              resolve: {
                  loadMyDirectives: function ($ocLazyLoad) {
                      return $ocLazyLoad.load({
                          name: 'chart.js',
                          files: [
                            'bower_components/angular-chart.js/dist/angular-chart.min.js',
                            'bower_components/angular-chart.js/dist/angular-chart.css'
                          ]
                      }), $ocLazyLoad.load(
                        {
                            name: 'yapp',
                            files: [
                          'scripts/controllers/solarsystem.js',
                          'scripts/directives/timeline/timeline.js',
                          'scripts/directives/notifications/notifications.js',
                          'scripts/directives/chat/chat.js',
                          'scripts/directives/dashboard/stats/stats.js',
                          'scripts/directives/dashboard/stats-1/stats.js',
                          'scripts/directives/dashboard/stats-2/stats.js'
                            ]
                        })
                  }
              }
          })
          .state('solarsystem.overview', {
              url: '/solarsystem/overview',
              parent: 'solarsystem',
              templateUrl: 'views/solarsystem/overview.html'
          })
          .state('solarsystem.reports', {
              url: '/solarsystem/reports',
              parent: 'solarsystem',
              templateUrl: 'views/solarsystem/reports.html'
          });
    
  });
