'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */

angular.module('yapp')
  .controller('SolarSysCtrl', ['$scope', '$state',  SolarSysCtrl]);

function SolarSysCtrl($scope, $state) {

    $scope.$state = $state;
    $scope.line = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        series: ['Series A', 'Series B'],
        data: [
	      [65, 59, 80, 81, 56, 55, 40],
	      [28, 48, 40, 19, 86, 27, 90]
        ],
        onClick: function (points, evt) {
            console.log(points, evt);
        }
    };
    $scope.toggle = function () {
        var menu = angular.element(document.querySelector('#sp-nav'));
        menu.toggleClass('show');
    }

    var app = angular.module('yapp', []);
    app.run(['$rootScope', '$spMenu', function ($rootScope, $spMenu) {
        $rootScope.$spMenu = $spMenu;
    }])
    .provider("$spMenu", function () {
        this.$get = [function () {
            var menu = {};

            menu.show = function show() {
                var menu = angular.element(document.querySelector('#sp-nav'));
                console.log(menu);
                menu.addClass('show');
            };

            menu.hide = function hide() {
                var menu = angular.element(document.querySelector('#sp-nav'));
                menu.removeClass('show');
            };

            menu.toggle = function toggle() {
                var menu = angular.element(document.querySelector('#sp-nav'));
                menu.toggleClass('show');
            };

            return menu;
        }];
    });
  };


