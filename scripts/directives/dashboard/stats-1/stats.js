'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('yapp')
    .directive('stats2',function() {
    	return {
  		templateUrl:'scripts/directives/dashboard/stats-1/stats.html',
  		restrict:'E',
  		replace:true,
  		scope: {
        'model': '=',
        'comments': '@',
        'number': '@',
        'name': '@',
        'colour': '@',
        'details':'@',
        'type':'@',
        'goto': '@',
        'PV': '@'
  		}
  		
  	}
  });
