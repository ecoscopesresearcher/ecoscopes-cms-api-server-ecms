'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('yapp')
    .directive('stats3',function() {
    	return {
  		templateUrl:'scripts/directives/dashboard/stats-2/stats.html',
  		restrict:'E',
  		replace:true,
  		scope: {
        'model': '=',
        'comments': '@',
        'number': '@',
        'name': '@',
        'colour': '@',
        'details':'@',
        'type':'@',
        'goto': '@',
        'PV': '@'
  		}
  		
  	}
  });
