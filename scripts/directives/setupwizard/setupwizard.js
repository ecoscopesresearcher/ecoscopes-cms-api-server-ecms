'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('yapp')
	.directive('setupwizard',function() {
    return {
        templateUrl:'scripts/directives/setupwizard/setupwizard.html',
        restrict: 'E',
        replace: true,
    }
  });
